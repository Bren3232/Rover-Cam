# Rover-Cam

Update April 19: See updated versioin on github https://github.com/Bren3232/Rover-Cam


Rover Cam is a network independent security, and pest control software, to be used on Raspberry Pi.  

On motion detection it can:  
    - record clips, or images  
    - play one or more audio files, or run python files  
    - control up to 8 GPIO pins by turning them on and off, called “Actions”  

With extra electronics and hardware, Rover Cam can be used for pest control, by making sounds that frighten, or irritate animals, by moving an object to scare the animal, or initiating a spray of water, as motion sensing sprinklers do. As a security camera it can sound an alarm, or any custom audio recording, and/or move objects.

Here's a quick look at the UI:<br/>
https://youtu.be/_9ljmr4q4M4

See manual for set-up.

